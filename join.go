package main

import (
	"sync"
)

type Join struct {
	lhsSort *SortTuple
	rhsSort *SortTuple
	outputChan chan<- int
}

func CreateJoin(lhs,rhs *SortTuple, output chan<- int) (j *Join) {
	j = &Join{}
	j.lhsSort = lhs
	j.rhsSort = rhs
	j.outputChan = output
	return
}

func (j *Join)filter(lid,rid int) bool {
	return j.lhsSort.buf[lid].b < j.rhsSort.buf[rid].b
}

func (j *Join) run() {
	wg := &sync.WaitGroup{}

	wg.Add(2)
	j.lhsSort.run(wg)
	j.rhsSort.run(wg)
	wg.Wait()

	result := 0
	lbuf := &j.lhsSort.buf
	rbuf := &j.rhsSort.buf
	lhsSize := len(j.lhsSort.buf)
	rhsSize := len(j.rhsSort.buf)

	lid, rid := 0, 0

	for ; lid< lhsSize ; lid++ {

		for ;rid<rhsSize && ((*lbuf)[lid].a > (*rbuf)[rid].a);rid++ {}

		if (rid==rhsSize) {
			break
		}

		for temprid := rid;
			temprid < rhsSize && (*lbuf)[lid].a == (*rbuf)[temprid].a;
		temprid++ {
			if (temprid == rhsSize) {
				break
			}

			if (j.filter(lid,temprid)) {
				result++
			}
		}
	}
	j.outputChan <- result
}