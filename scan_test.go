package main

import "testing"

func TestSeqScan(t *testing.T) {
	table := [][]string{{"1", "1"}, {"2", "2"}}
	scan := CreateScan(table)

	scan.outputChan = append(scan.outputChan, make(chan *Tuple, 10))
	scan.SeqScan()

	result1 := Tuple{1, 1}
	result2 := Tuple{2, 2}

	ret := <-scan.outputChan[0]
	if result1 != *ret {
		t.Errorf("testSlice 0 error, %+v", ret)
	}

	ret = <-scan.outputChan[0]
	if result2 != *ret {
		t.Errorf("testSlice 1 error, %+v", ret)
	}
}
