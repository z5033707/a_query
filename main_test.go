package main

import (
	"testing"
)

func TestInitContext(t *testing.T) {
	if context != nil {
		t.Errorf("context is not nil in very beginning, %+v", context)
	}

	c := InitContext()

	if c != context {
		t.Errorf("context is not singleton, context: %+v, ret: %+v ", context, c)
	}

	c1 := InitContext()

	if c != c1 {
		t.Errorf("context is not singleton, first: %+v, second: %+v ", c, c1)
	}
}

