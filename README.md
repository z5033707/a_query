###实现
整体上来说我使用了将数据按照a为分布键进行重分布到32个goroutine中并行进行处理的方式。query的处理是在每个partition中将数据进行排序后进行mergejoion最后在gather到主线程中得到结果。


我采用了merge join的方式处理这个query，在做排序时采用非叶子节点merge sort叶子节点quicksort的方式进行排序。排序键为a,b。按照 `v1.a < v2.a) || (v1.a == v2.a && v1.b < v2.b` 的规则进行递增排序。因为在读数据的时候我将 t1 表和 t2表进行了交换，这样我就可以将query改成`select count(*) from t1 join t2 on t1.a = t2.a and t1.b < t2.b`.
###3. 请分析并测试各个表中 a 的 number of distinct values 对该算法性能的影响

如果只是a的distinct value 减小的话对算法性能下降的影响较小，性能主要影响在数据重分布的时候大量数据会重分布到一个partition中进行排序。但由于我的排序也是并行执行的，所以对排序的性能影响相对较小。


###4. 请思考并列举出所有能想到的做法，越多越好。描述他们的优劣点，适用的场景，什么场景下执行的快，什么场景下执行的慢
对于join可选的方案就是hash join， merge join以及nestloop
如果说两张表一张大表一张小表的话，我们可以用小表做hashtable，让大表做外表

merge join 需要保证作为输入的两张表数据是有序的，如果说我们在存文件的时候本身数据就已经有序了那么我们就可以不用在merge join下面做sort操作，这时性能会比hash join要好。另外，如果join节点上边有order by 这样的操作，mergejoin还是有优势的，mergejoin的结果还是有序的


另外，在做这个作业的时候，一张表所有的数据都放在了一个文件里，读取文件并且parser成 <int,int>元祖实际上是瓶颈，如果可以将一个表的数据均匀的存在若干个文件中，scan的效率会更高

