package main

import (
	"sort"
	"sync"
)

type SortTuple struct {
	buf []*Tuple
	inputChan chan *Tuple
}

func merge(s1ch, s2ch <-chan *Tuple, outChan chan<- *Tuple) {
	update := func (input <-chan *Tuple, output chan<- *Tuple, t **Tuple, ok *bool) {
		output <- *t
		*t, *ok = <-input
	}

	v1, ok1 := <-s1ch
	v2, ok2 := <-s2ch

	for ok1 && ok2 {
		if (v1.a < v2.a) || (v1.a == v2.a && v1.b < v2.b) {
			update(s1ch, outChan, &v1, &ok1)
		} else {
			update(s2ch, outChan, &v2, &ok2)
		}
	}

	for ok1 {
		update(s1ch, outChan, &v1, &ok1)
	}

	for ok2 {
		update(s2ch, outChan, &v2, &ok2)
	}
}
const KB = 1024
const MB = 1024 * KB

func SortSlice(buf []*Tuple, outChan chan<- *Tuple) {
	defer close(outChan)
	if (len(buf) < MB) {
		less := func(i, j int) bool {
			return (buf[i].a < buf[j].a) || (buf[i].a == buf[j].a && buf[i].b < buf[j].b)
		}
		sort.Slice(buf, less)

		for _,v := range buf {
			outChan <- v
		}

		return
	}

	mid := len(buf) / 2
	s1 := make(chan *Tuple, mid)
	s2 := make(chan *Tuple, len(buf)-mid)

	go SortSlice(buf[:mid], s1)
	go SortSlice(buf[mid:], s2)

	merge(s1, s2, outChan)
}

func (s *SortTuple) run(wg *sync.WaitGroup) {
	//build buf for rank
	inputBuf := []*Tuple{}
	for t := range s.inputChan {
		inputBuf = append(inputBuf, t)
	}

	ch := make(chan *Tuple,len(inputBuf))
	go SortSlice(inputBuf, ch)

	for v := range ch {
		s.buf = append(s.buf, v)
	}

	wg.Done()
}

func CreateSort(chanSize int) (s *SortTuple) {
	s = &SortTuple{buf: []*Tuple{}, inputChan: make(chan *Tuple, chanSize)}

	return s
}
