package main

import (
	"fmt"
	"sync"
)

type SyncMgr struct {
	chanMap map[chan *Tuple] int
	mux sync.Mutex
	partitionNum int
}

var syncManager *SyncMgr;

func CreateSyncMgr(partitionNum int) *SyncMgr{
	s := &SyncMgr{}
	s.chanMap = make(map[chan *Tuple]int)
	s.partitionNum = partitionNum

	syncManager = s

	return syncManager
}

func (sm *SyncMgr)RegisteChan(c chan *Tuple) {
	sm.mux.Lock()
	defer sm.mux.Unlock()

	if (sm == nil) {
		fmt.Errorf("SyncMgr is nil")
	}
	sm.chanMap[c] += 1
}

func (sm *SyncMgr)UnregisteChan(c chan *Tuple) {
	sm.mux.Lock()
	defer sm.mux.Unlock()

	if (sm == nil) {
		fmt.Errorf("SyncMgr is nil")
	}
	sm.chanMap[c] -= 1

	if (sm.chanMap[c] == 0) {
		close(c)
	}
}
