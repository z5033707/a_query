package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	_ "net/http/pprof"
	"os"
	"runtime/pprof"
	"strings"
	"sync"
	"time"
)

const PATHNUMBER = 64
type Tuple struct {
	a int
	b int
}

type Context struct {
	tab1        [][]string   `t1 raw data`
	t1ChanSlice []chan *Tuple `parse t1 raw data to Tuple then send to join slice`

	tab2        [][]string   `t2 raw data`
	t2ChanSlice []chan *Tuple `parse t2 raw data to Tuple then send to join slice`

	PathNum int `number of process path`

	//For debug used
	CSVParserStart time.Time
	QueryStart time.Time
	QueryFinish time.Time
}

type Agg struct {
	inputChan chan int;
	joinSlice [] *Join
	scan1Slice [] *Scan `all scan for table 1, it is a little bit hack, but fine for demo`
	scan2Slice [] *Scan `all scan for table 2, it is a little bit hack, but fine for demo`
}

var context *Context
var once sync.Once

func (a *Agg) run() int {
	result := 0

	// run scan routine
	for _,v := range a.scan1Slice {
		go v.SeqScan()
	}

	for _,v := range a.scan2Slice {
		go v.SeqScan()
	}

	// run join routine
	for _,v := range a.joinSlice {
		go v.run()
	}

	// gather result
	for i := 0; i < context.PathNum; i++ {
		result += <-a.inputChan
	}

	return result
}

/*
* Hard code to read all data in t1.csv and t2.csv
 */
func readCSV() ([][]string, [][]string) {

	var ss1,ss2 [][]string

	cntb1, err1 := ioutil.ReadFile("t1.csv")
	if err1 != nil {
		panic(err1.Error())
	}
	r1 := csv.NewReader(strings.NewReader(string(cntb1)))

	cntb2, err2 := ioutil.ReadFile("t2.csv")
	if err2 != nil {
		panic(err2.Error())
	}
	r2 := csv.NewReader(strings.NewReader(string(cntb2)))

	wg := &sync.WaitGroup{}
	wg.Add(2)
	go func() {
		defer wg.Done()
		ss1, _ = r1.ReadAll()
	}()

	go func() {
		defer wg.Done()
		ss2, _ = r2.ReadAll()
	}()

	wg.Wait()

	return ss1, ss2
}

// InitContext read csv files and create channel for sharding data
func InitContext() (c *Context) {
	once.Do(func() {
		if context == nil {
			c = &Context{PathNum: PATHNUMBER}
			c.CSVParserStart = time.Now();
			c.tab1, c.tab2 = readCSV()

			// swap table so that query filter can change to t1.b < t2.b
			// It is no different for query result but easy for me coding sort
			// I can rank data by a,b increasingly
			c.tab1, c.tab2 = c.tab2,c.tab1
			context = c

			CreateSyncMgr(c.PathNum)
		}
	})

	return context
}

// CreateExecutor
func CreateExecutor(c *Context) (*Agg) {
	step1 := len(c.tab1) / c.PathNum
	step2 := len(c.tab2) / c.PathNum
	begin1:= 0
	begin2:= 0
	var end1,end2 int

	scan1Slice := [] *Scan{}
	scan2Slice := [] *Scan{}

	sort1Slice := [] *SortTuple{}
	sort2Slice := [] *SortTuple{}

	sort1ChanSize := len(c.tab1) / c.PathNum
	sort2ChanSize := len(c.tab2) / c.PathNum

	for i:= 0; i < c.PathNum; i++ {

		if (i < c.PathNum - 1) {
			end1 = begin1 + step1
			end2 = begin2 + step2
		} else {
			end1 = len (c.tab1)
			end2 = len (c.tab2)
		}

		scan1Slice = append(scan1Slice, CreateScan(c.tab1[begin1:end1]))
		scan2Slice = append(scan2Slice, CreateScan(c.tab2[begin2:end2]))

		sort1Slice = append(sort1Slice, CreateSort(sort1ChanSize))
		sort2Slice = append(sort2Slice, CreateSort(sort2ChanSize))
		begin1 = end1
		begin2 = end2
	}

	agg := &Agg{inputChan: make(chan int, c.PathNum)}

	agg.joinSlice = [] *Join{}
	for i:= 0; i < c.PathNum; i++ {
		scan1Slice[i].bindSort(sort1Slice)
		scan2Slice[i].bindSort(sort2Slice)

		agg.joinSlice = append(agg.joinSlice, CreateJoin(sort1Slice[i], sort2Slice[i], agg.inputChan))
	}

	agg.scan1Slice = scan1Slice
	agg.scan2Slice = scan2Slice

	return agg
}

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")
var memprofile = flag.String("memprofile", "", "write memory profile to this file")
func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}


	/* init */
	ctx := InitContext()
	agg := CreateExecutor(ctx)

	/* SELECT count(*) from t1 join t2 on t1.a = t2.a and t1.b > t2.b */

	context.QueryStart = time.Now();

	result := agg.run()

	fmt.Println("result = ", result);

	context.QueryFinish= time.Now();

	fmt.Println("CSV Parser Time:", context.QueryStart.Sub(context.CSVParserStart));
	fmt.Println("Query Execution Time:", context.QueryFinish.Sub(context.QueryStart));
	fmt.Println("Total Time:", context.QueryFinish.Sub(context.CSVParserStart));

	if *memprofile != "" {
		f, err := os.Create(*memprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.WriteHeapProfile(f)
		f.Close()
		return
	}
}
