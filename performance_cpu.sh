#!/bin/bash
go build
./pingcap_query --cpuprofile=pingcap_query.prof
go tool pprof -web pingcap_query pingcap_query.prof
