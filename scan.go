package main

import (
	"strconv"
)

type Scan struct {
	table [][]string

	tupleBuf []Tuple
	outputChan []chan *Tuple
}

// SeqScan parser two-dimension string from `from` to `to`, translate to
// our Tuple, send to relative channel by hash.
func (s *Scan) SeqScan() {

	// create all tuples memory once
	tableSize := len(s.table)
	tupleBuf := make([]Tuple, tableSize)

	for i := 0 ; i < tableSize; i ++ {

		// trans string to int
		tupleBuf[i].a, _ = strconv.Atoi(s.table[i][0])
		tupleBuf[i].b, _ = strconv.Atoi(s.table[i][1])

		// redistribute to a partition by (a)
		id := tupleBuf[i].a % len(s.outputChan)
		s.outputChan[id] <- &tupleBuf[i]
	}

	for _,v := range s.outputChan {
		syncManager.UnregisteChan(v)
	}
}

func CreateScan(tab [][]string) (s *Scan) {
	s = &Scan{table: tab}

	return
}

func (s *Scan) bindSort(sortSlice []*SortTuple) {

	for _, v := range sortSlice {
		s.outputChan = append(s.outputChan, v.inputChan)
		syncManager.RegisteChan(v.inputChan)
	}

}
